# Meson with Googltest 

1. Create a subprojects folder `mkdir subprojects`
2. Install the gtest wrap `meson wrap install gtest`
3. Install the gtest library (on unbuntu) `apt install libgtest-dev`
4. Setup the build folder `meson build`
5. Build the code `cd build && ninja`
6. Run the tests `./tests/testprof` 
 
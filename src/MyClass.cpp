
#include "MyClass.hpp"
#include <stdexcept>

int MyClass::fact(const int N)
{
    if (N < 0)
    {
        throw std::invalid_argument("negative factorial not allowed");
    } // end if
    else if (N == 0 || N == 1)
    {
        return 1;
    } // end elif

    return N * fact(N-1);   
} // end method fact

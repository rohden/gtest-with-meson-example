
#include <gtest/gtest.h>
#include <stdexcept>
#include <cmath>

#include "MyClass.hpp"


TEST(factorial_test, negative) 
{
    ASSERT_THROW(MyClass::fact(-5), std::invalid_argument) << "Factorial did not throw with a negative argument";
}

TEST(factorial_test, zero) 
{
    ASSERT_EQ(MyClass::fact(0), 1) << "Factorial of 0 did not yield 1";
}

TEST(factorial_test, one) 
{
    ASSERT_EQ(MyClass::fact(1), 1) << "Factorial of 1 did not yield 1";
}

TEST(factorial_test, positive) 
{
    ASSERT_EQ(MyClass::fact(2), 2)    << "fact(2) did not equal gamma(2)";
    ASSERT_EQ(MyClass::fact(3), 6)    << "fact(3) did not equal gamma(6)";
    ASSERT_EQ(MyClass::fact(4), 24)   << "fact(4) did not equal gamma(24)";
    ASSERT_EQ(MyClass::fact(5), 120)  << "fact(5) did not equal gamma(120)";
    ASSERT_EQ(MyClass::fact(6), 720)  << "fact(6) did not equal gamma(720)";
    ASSERT_EQ(MyClass::fact(7), 5040) << "fact(7) did not equal gamma(5040)";
}